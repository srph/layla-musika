## Layla
[Layla](https://www.facebook.com/LaylaMusika/) [Lie-La], consists of four members that are driven by their urge to experiment and explore other genres. Despite their different influences, they managed to fuse the styles of each of their influences to produce the music that satisfies their ears.

### PROJECT 101
I started this project for studying purposes (and was brought to github to share to other people), however this might be used for production in the future.

Currently up @ [http://laylaprojek.comuv.com]
