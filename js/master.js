$(document).ready(function() {
	var menu = $('.menu');
	// Scroll to a page
	function scrollTo(id) {
		var page = $('html, body');
		// Wrap given parameters in jQuery
		id = $(id);

		// Scroll
		page.animate({
			scrollTop: id.offset().top - ( menu.height() )
		}, 'slow');
	}

	var nav = $('.navigation li a');
	nav.on('click', function(event) {
		// Load the DOM
		link = $(this).attr('href');
		// Prevent default action
		event.preventDefault();
		// Scroll
		scrollTo(link);
	});

	// Sticky a scroll bar after certain point
	function StickyNav() {
		$(window).scroll(function() {
			// Check the DOM
			var win = $(window);
				winHeight = win.height(),
				scrollTop    = win.scrollTop(),
				position     = menu.offset().top,
				elementHeight = menu.height(),
				$this = $(this);

			if ( scrollTop > winHeight ) {
				console.log('fix');
				menu.css({
					position: 'fixed',
					top: 0
				});
			} else {
				menu.css({
					position: 'absolute',
					top: winHeight - elementHeight
				});
			}
		});
	}

	StickyNav();




});